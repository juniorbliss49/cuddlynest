import React from 'react';

import './SideDrawer.scss';
import Backdrop from '../Backdrop/Backdrop';

const sideDrawer = ( props ) => {
    const {image} = props
    console.log(image? image : null)
    let attachedClasses = ['SideDrawer', 'Close'];
    if (props.open) {
        console.log('yes open')
        attachedClasses = ['SideDrawer', 'Open'];
    }
    return (
        <>
            <Backdrop show={props.open}  />
            <div className={attachedClasses.join(' ')}>
                <div className="SideDrawer--close">
                    <img src="https://www.cuddlynest.com/images/ldv2_images/close_slider.png" onClick={props.closed} />
                </div>
                <div className="SideDrawer--carousel">
                    <div className="SideDrawer--carousel__prev">
                        <img src="https://www.cuddlynest.com/images/ldv2_images/arrow_prev.svg" onClick={()=>props.prevPhoto()} />
                    </div>
                    <div className="SideDrawer--carousel__cont">
                        <img {...image[0]} className="SideDrawer--carousel__cont-img"  />
                    </div>
                    <div className="SideDrawer--carousel__next">
                        <img src="https://www.cuddlynest.com/images/ldv2_images/arrow_next.svg" onClick={()=>props.nextPhoto()} />
                    </div>
                </div>
                <div className="SideDrawer--btmDetail">
                    <p>{props.currentNum} / {props.totalNum}</p>
                </div>
                

            </div>
        </>
    );
};

export default sideDrawer;