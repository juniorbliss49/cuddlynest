import React from "react";
import "./Footer.scss";

const Footer = props =>{
    return (
        <footer className="container-fluid">
            <div className="footer">

                <section className="footer--top">
                    <div className="footer--side">
                        <h5>Travel is for <br /> everyone <span className="footer--dot">.</span></h5>
                        <br /><br /><br />
                        <div className="footer--side__social">
                            <img src="./youtube.svg" />
                            <img src="./twitter.svg" />
                            <img src="./facebook.svg" />
                            <img src="./instagram.svg" />
                            <img src="./pinterest.svg" />
                        </div>
                    </div>
                    <div className="footer--section">
                        <div className="footer--section__links">
                            <ul>
                                <li>
                                    <a>About us</a>
                                </li>
                                <li>
                                    <a>Help</a>
                                </li>
                                <li>
                                    <a>Blog</a>
                                </li>
                                <li>
                                    <a>Privacy policy</a>
                                </li>
                                <li>
                                    <a>Terms and conditions</a>
                                </li>
                            </ul>
                        </div>
                        <div className="footer--section__links">
                            <ul>
                                <li>
                                    <a>Press releases</a>
                                </li>
                                <li>
                                    <a>Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
                <br /><br /><br /><br /><br />
                <section className="footer--bottom">
                    <div className="footer--side">
                        <p>CuddlyNest © 2020.</p>
                        <p>Made with <img src="./heart.svg" className="img" /> from Chicago</p>
                    </div>
                    <div className="footer--content">
                        <p className="footer--content__text">CuddlyNest is a member of <span> </span></p>
                    </div>
                </section>
                
            </div>
        </footer>
    )
}

export default Footer;