import React from "react";
import NavBar from "./NavBar/NavBar";
import HotelPhotos from "./HotelPhotos/HotelPhotos";
import "./Header.scss";

const Header =props=>{
    return (
        <div className="Header">
            <NavBar />
            <HotelPhotos selectedImg={props.choosePix} photoGallery={props.gallery} />
        </div>
    )
}

export default Header;