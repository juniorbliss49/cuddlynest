import React from "react";
import "./Logo.scss";

const Logo = props =>{
    return (
        <div className="Logo">
            <img src="https://www.cuddlynest.com/images/logo/cn_logo_hpv2_clor_en.png" className="Logo--img" />
        </div>
    )
}

export default Logo;