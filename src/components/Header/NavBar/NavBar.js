import React from "react";
import Logo from "./Logo/Logo";
import SearchBar from "./SearchBar/SearchBar";
import Nav from "./Nav/Nav";
import "./NavBar.scss";

const NavBar = props =>{
    return (
        <div className="NavBar">
            <Logo />
            <SearchBar />
            <Nav />
        </div>
    )
}

export default NavBar;