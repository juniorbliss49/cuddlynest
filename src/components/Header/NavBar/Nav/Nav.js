import React from "react";
import "./Nav.scss";

const Nav = props =>{
    return (
        <div className="Nav">
            <ul className="Nav--section">
                <li className="Nav--section__list">
                    <a className="Nav--section__list_link_dropdown">USD</a>
                    <ul className="Nav--section__list_link_sub">
                        <li>
                        <ul>
                            <li><a className="active">US Dollar</a></li>
                            <li><a>Euro</a></li>
                            <li><a>Pound sterling</a></li>
                            <li><a>Chinese yuan</a></li>
                        </ul>  
                        </li>
                        <li className="Nav--section__list_link_sub_menu">
                        <ul >  
                            <li><a>Emirati dirham</a></li>
                            <li><a>Argentine peso</a></li>
                            <li><a>Australian dollar</a></li>
                            <li><a>Brazilian real</a></li>
                        </ul>  
                        </li>
                    </ul>
                </li>
                <li className="Nav--section__list">
                    <a className="Nav--section__list_link">Log in</a>
                </li>
                <li className="Nav--section__list">
                    <a className="Nav--section__list_link">Help</a>
                </li>
                <li className="Nav--section__list">
                    <a className="Nav--section__list_link">List your property</a>
                </li>
            </ul>
        </div>
    )
}

export default Nav;