import React from "react";
import "./SearchBar.scss";

const SearchBar = props =>{
    return (
        <div className="SearchBar">
            <input type="text" placeholder="Search your destination here.." className="SearchBar--input" />
        </div>
    )
}

export default SearchBar;