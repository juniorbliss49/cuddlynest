import React from "react";
import "./HotelPhotos.scss";

const HotelPhotos =props=>{
    return (
        <div className="HotelPhotos">
            <div className="HotelPhotos--main" >
                <img src="https://img.cuddlynest.com/images/listings/2019/08/01/16/5914c4a39ddd09c3e15c802d26b46c2f.jpg" onClick={()=>props.selectedImg(1)} className="HotelPhotos--main__img" />
            </div>
            <div className="HotelPhotos--sub">
                <div className="HotelPhotos--sub__img">
                    <img src="https://img.cuddlynest.com/images/listings/2019/08/01/16/d1da5d7bf5b5ab2812a177fa3b84d607.jpg" onClick={()=>props.selectedImg(2)} className="HotelPhotos--sub__img-main" />
                </div>
                <div className="HotelPhotos--sub__img">
                    <img src="https://img.cuddlynest.com/images/listings/2019/08/01/16/9aff36887731f43c8cd9d36a5bc09c8b.jpg" onClick={()=>props.selectedImg(3)}  className="HotelPhotos--sub__img-main" />
                </div>
                <div className="HotelPhotos--sub__img">
                    <img src="https://img.cuddlynest.com/images/listings/2019/08/01/16/60a2c0f3200c93c1cea272143700fa64.jpg" onClick={()=>props.selectedImg(4)}  className="HotelPhotos--sub__img-main" />
                </div>
                <div className="HotelPhotos--sub__img">
                    <img src="https://img.cuddlynest.com/images/listings/2019/08/01/16/d214582531aecb4059f6a17cfdb88709.jpg" onClick={()=>props.selectedImg(5)} className="HotelPhotos--sub__img-main" />
                </div>
            </div>
            <div className="HotelPhotos--button">
                <button className="HotelPhotos--button__text">Show all photos</button>
            </div>
            <div className="HotelPhotos--hidden">
                    <button className="HotelPhotos_Btn2">Share </button>
                    <button className="HotelPhotos_Btn">Save to wishlist</button>
            </div>
            
        </div>
    )
}

export default HotelPhotos;