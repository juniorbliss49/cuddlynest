import React, { useState } from "react";
import  "./Main.scss";
import MapContainer  from "./MapContainer/MapContainer";
import { Link, animateScroll as scroll } from "react-scroll";
import { DatePicker,RangeDatePicker } from '@y0c/react-datepicker';
import '@y0c/react-datepicker/assets/styles/calendar.scss';
import GoogleMapReact from 'google-map-react';

const center = {
    lat: 59.95,
    lng: 30.33
  }
  const AnyReactComponent = ({ text }) => <div>{text}</div>;

const Main = props =>{
    const [guests, setGuests] = useState(1)

    const addGuest = ()=>{
        setGuests(prevGuest=>{
           return prevGuest + 1
        })
    }

    const minusGuest = ()=>{
        setGuests(prevGuest=>{
           return prevGuest + 1
        })
    }
    
    return (
        <section className="Main">
            <div className="Main--header">
                <div className="Main--header__title">
                    <h1 className="Main--header__title_h1">Hipotel Paris Hippodrome Parc Floral</h1>
                    <span className="Main--header__title-span">Hotel</span>
                </div>
                <div className="Main--header__fav">
                    <button className="Main--header__fav_Btn">Save to wishlist</button>
                    <button className="Main--header__fav_Btn2">Share this hotel</button>
                </div> 
            </div>
            
            <div className="Hotel">
                <div className="Hotel--header">
                    <ul className="Hotel--header__ul">
                        <li className="Hotel--header__ul_li">
                                <Link
                                    activeClass="active"
                                    to="select_rooms"
                                    spy={true}
                                    smooth={true}
                                    offset={-70}
                                    duration={500}
                                >
                                    Select rooms
                                </Link>
                            
                        </li>
                        <li className="Hotel--header__ul_li">
                                <Link
                                    activeClass="active"
                                    to="location"
                                    spy={true}
                                    smooth={true}
                                    offset={-70}
                                    duration={500}
                                >
                                    Location
                                </Link>
                        </li>
                        <li className="Hotel--header__ul_li">
                            <Link
                                    activeClass="active"
                                    to="description"
                                    spy={true}
                                    smooth={true}
                                    offset={-70}
                                    duration={500}
                                >
                                    Description
                                </Link>
                                
                        </li>
                        <li className="Hotel--header__ul_li">
                            <Link
                                    activeClass="active"
                                    to="policies"
                                    spy={true}
                                    smooth={true}
                                    offset={-70}
                                    duration={500}
                                >
                                    Accomodation policies
                                </Link>
                                
                        </li>
                    </ul>
                </div>
                <br /><br />
                <div className="Hotel--selectRoom" id="select_rooms">
                    <div className="Hotel--selectRoom__datePicker">
                        <RangeDatePicker  startPlaceholder="Check-in" endPlaceholder="Check-out" />
                        <div className="Hotel--selectRoom__datePicker--number">
                            <button onClick={minusGuest}>-</button>
                            {guests} Guests
                            <button onClick={addGuest}>+</button>
                        </div>
                    </div>
                    <div className="Page--Warning">
                        <p className="Page--Warning__text">No room available for these dates. Please try another date.</p>
                    </div>
                </div>
                <br /><br />
                <div className="Hotel-location" id="location">
                    
                    <h3 className="Hotel--h3">Location</h3>
                    <p className="Hotel--p">1 Allée Edmé L'heureux, Joinville-le-Pont, Île-de-France, 94340, France</p>
                    <div className="Hotel--map">
                    <GoogleMapReact
                    bootstrapURLKeys={{ key: 'AIzaSyAYdSQ2ggSR4vczJLqGaX628Ua-1JVESyI' }}
                    defaultCenter={center}
                    defaultZoom={11}
                    >
                    <AnyReactComponent
                        lat={59.955413}
                        lng={30.337844}
                        text="My Marker"
                    />
                    </GoogleMapReact>
                    </div>
                </div>
                <br />
                <div className="Hotel--description" id="description">
                    <h3 className="Hotel--h3">Description </h3>
                    <h4 className="Hotel--h4">The Space</h4>
                    <p className="Hotel--p">Conveniently located near the Joinville-le-Pont RER station, Hipotel Paris Hippodrome Parc Floral Jointville-le-Pont offers quick and easy access to Paris. It features comfortable and good value accommodation.</p>
                    <p className="Hotel--p">The rooms at the Hipotel are homey and fitted with all the facilities you will need including tea and coffee making facilities.</p>
                    <p className="Hotel--p">The generous buffet breakfast is served daily in the dining room, and the restaurant proposes buffet-style traditional French cuisine.</p>
                    <p className="Hotel--p">The Hipotel Paris Hippodrome boasts a peaceful setting near the Hippodrome de Vincennes, the parks and forest.</p>
                </div>
                <br />
                <div className="Hotel-policies" id="policies">
                    <h3 className="Hotel--h3">Accommodation policies</h3>
                    <table className="Hotel--table">
                        <tr>
                            <td>Checkin</td>
                            <td>2:00 PM</td>
                        </tr>
                        <tr>
                            <td>Checkout</td>
                            <td>12:00 PM (noon)</td>
                        </tr>
                        <tr>
                            <td>Cancellation</td>
                            <td>Cancellation policy varies with the check-in dates. Please select the checkin/checkout date to see the cancellation policy for those dates.
                            CuddlyNest service fee is non-refundable.
                            </td>
                        </tr>
                        <tr>
                            <td>Minimum Stay</td>
                            <td>1 night</td>
                        </tr>
                        <tr>
                            <td>Rules</td>
                            <td>Meal: Buffet breakfast EUR 7
                                Pet: Pets are not allowed.</td>
                        </tr>
                        <tr>
                            <td>Important notices</td>
                            <td>For hotels that accept children, local regulations state that guests under 18 cannot stay alone.</td>
                        </tr>
                    </table>
                </div>
                <br /><br />
                <section className="Hotel--report">
                    <button><img src="https://www.cuddlynest.com/images/hpv2_homeicons/flag_v2.png?v=0.0" /> Report this hotel</button>
                </section>
            </div>
            <br />
        </section>
    );
}

export default Main;