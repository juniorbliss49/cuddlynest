import React, { Component } from "react";
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

const mapStyles = {
    width: '100%',
    height: '100%'
  };

export class MapContainer extends Component {
  render() {
    return (
      <Map 
      google={this.props.google} 
      zoom={14}
      style={mapStyles}
      initialCenter={{
        lat: 40.854885,
        lng: -88.081807
      }}>
 
      
 
        
      </Map>
    );
  }
}
 
export default GoogleApiWrapper({
  apiKey: 'AIzaSyAYdSQ2ggSR4vczJLqGaX628Ua-1JVESyI'
})(MapContainer)