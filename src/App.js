import React, { Component } from 'react';
import logo from './logo.svg';
import './App.scss';
import Header from './components/Header/Header';
import Main from './components/Main/Main';
import Footer from './components/Footer/Footer';
import SideDrawer from './components/SideDrawer/SideDrawer';

class App extends Component {
  state = {
    showSideDrawer: false,
    photoGallery: [
      {
        id: 1,
        src: "https://img.cuddlynest.com/images/listings/2019/08/01/16/5914c4a39ddd09c3e15c802d26b46c2f.jpg"
      },
      {
        id: 2,
        src: "https://img.cuddlynest.com/images/listings/2019/08/01/16/d1da5d7bf5b5ab2812a177fa3b84d607.jpg"
      },
      {
        id: 3,
        src: "https://img.cuddlynest.com/images/listings/2019/08/01/16/9aff36887731f43c8cd9d36a5bc09c8b.jpg"
      },
      {
        id: 4,
        src: "https://img.cuddlynest.com/images/listings/2019/08/01/16/60a2c0f3200c93c1cea272143700fa64.jpg"
      },
      {
        id: 5,
        src: "https://img.cuddlynest.com/images/listings/2019/08/01/16/d214582531aecb4059f6a17cfdb88709.jpg"
      },
      {
        id: 6,
        src: "https://img.cuddlynest.com/images/listings/2019/10/05/04/41936afe12c74b91e45766642f7a8198.jpg"
      },
      {
        id: 7,
        src: "https://img.cuddlynest.com/images/listings/2019/10/05/04/73d6fe25f6584483e1109f98bdc8d11e.jpg"
      },
      {
        id: 8,
        src: "https://img.cuddlynest.com/images/listings/2019/12/12/00/04eb49f3f8f9fd5927f4bbcb6c614bab.jpg"
      },
    ],
    selectedImage: {
      id: 0,
      src: ""
    },
    iniTialState: 1,
}

sideDrawerClosedHandler = () => {
    this.setState( { showSideDrawer: false } );
}

sideDrawerToggleHandler = () => {
    this.setState( ( prevState ) => {
        return { showSideDrawer: !prevState.showSideDrawer };
    } );
}

showPix = (id)=>{
  this.sideDrawerToggleHandler()
  const photo = this.state.photoGallery.filter(item => {
    return item.id == id
  })
  this.setState({selectedImage: photo})
  this.setState({iniTialState: id})
  
}

nextImage = ()=>{
  const id = this.state.iniTialState + 1;
  if(id <= this.state.photoGallery.length){
  const photo = this.state.photoGallery.filter(item => {
    return item.id == id
  })
  this.setState({selectedImage: photo})
  this.setState({iniTialState: id})
}
}

prevImage = ()=>{
  const id = this.state.iniTialState - 1;
  if(id >= 1){
  const photo = this.state.photoGallery.filter(item => {
    return item.id == id
  })
  this.setState({selectedImage: photo})
  this.setState({iniTialState: id})
  }
}

  render(){
    return (
      <>
      <SideDrawer
        open={this.state.showSideDrawer}
        closed={this.sideDrawerClosedHandler}
        nextPhoto={this.nextImage}
        prevPhoto={this.prevImage}
        image={this.state.selectedImage}
        currentNum={this.state.iniTialState}
        totalNum={this.state.photoGallery.length} />
        <div className="container">
          <Header choosePix={this.showPix} gallery={this.state.photoGallery} />
          <Main  />
        </div>
        <Footer />
      </>
    );
  }
}

export default App;
